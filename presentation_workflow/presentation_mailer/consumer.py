import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("Received: %r" % body)
    dict_body = json.loads(body)
    send_mail(
        "Your presentation has been approved",
        f"{dict_body['presenter_name']}, we're happy to tell you that your presentation {dict_body['title']} has been approved",
        "admin@conference.go",
        [f"{dict_body['presenter_email']}"],
        fail_silently=False,
    )


def process_rejected(ch, method, properties, body):
    dict_body = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f"{dict_body['presenter_name']}, we're sorry to inform you that your presentation {dict_body['title']} has been rejected",
        "admin@conference.go",
        [f"{dict_body['presenter_email']}"],
        fail_silently=False,
    )


while True:
    try:

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejected,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
